package role

import (
	"encoding/json"
	"fmt"

	"gitee.com/meta-liam/data-center/database/redis"
	"gorm.io/gorm"
)

const redisSecondTime = 32

func (e *SrvRole) CacheMulti(DB *gorm.DB) ([]*Model, error) {
	key := fmt.Sprintf("%s_rb_role_ls", redis.GetConfig().RedisPrefix)
	v, err := redis.GetServer().GetBytes(key)
	var got []*Model
	var err2 error
	if err == nil {
		err2 = json.Unmarshal(v, &got)
		if err2 == nil {
			//println("redis data")
			return got, nil
		}
	}
	got, _ = e.FindMultiByNil(DB)
	if len(got) > 0 {
		byteValue, _ := json.Marshal(&got)
		redis.GetServer().Set(key, byteValue, redisSecondTime)
	}
	return got, nil
}
