module gitee.com/meta-liam/data-center

go 1.16

require (
	github.com/go-liam/util v1.1.3
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gs-mblock/mbgo v1.0.8
	github.com/timest/env v0.0.0-20180717050204-5fce78d35255
	gorm.io/gorm v1.24.0
)
