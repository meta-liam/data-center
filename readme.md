# Go的工具包

> 基于goland语言的工具包

## go环境

 go 1.16

## 单元测试

```shell script
go test -coverpkg=./... -coverprofile=coverage.data ./...
```

## 安装

```shell
go get gitee.com/meta-liam/data-center
```

## 兼容

 go1.16
